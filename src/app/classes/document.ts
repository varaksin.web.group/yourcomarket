export class Document {
  body!: string;
  price!: number;
  catTitle!: string;

  constructor(public id: number, public catId: number, public title: string) {
  }
}
