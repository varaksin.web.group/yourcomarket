export class Questions {
    questions!: Question[];

    constructor(
        public title: string,
        public body: string) {}
}

export class Question {
    constructor(
       public quest: string,
       public answer: string
    ) {}
}
