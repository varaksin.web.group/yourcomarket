export class DinamicFormFieldItem {

    constructor(
        public value: string,
        public key: string,
        public  label: string,
        public type: string,
        public controlType?: string,
        public  required?: string,
        public   order?: number,
    ) {
    }
}
