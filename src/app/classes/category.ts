export class Category {

    // desc?: string;
    children?: Category[];


    constructor(
        public tid: number,
        public name: string,
        public desc?: string,
    ) {
    }
}
