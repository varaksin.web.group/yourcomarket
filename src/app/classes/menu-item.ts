export class MenuItem {
    public attributes?: any[];
    constructor(
        public id: string,
        public text: string,
        public href: string
        ) {
    }
}
