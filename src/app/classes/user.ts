export class User {

    constructor(
        public uid: number,
        public mail: string,
        public name: string,
    ) {
    }
}
