import {Injectable} from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class GlobalInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

        const token = localStorage.getItem('jwt-token');
        if (token) {
            const authReg = request.clone({
                headers: request.headers.append('Authorization', 'Bearer ' + token)
            });
            return next.handle(authReg);
        }
        return next.handle(request);


    }
}
