import { TestBed } from '@angular/core/testing';

import { ExtractDocumentService } from './extract-document.service';

describe('ExtractDocumentService', () => {
  let service: ExtractDocumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExtractDocumentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
