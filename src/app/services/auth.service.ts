import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

const PATH = environment.restApiUrl;

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) {
    }


    // tslint:disable-next-line:typedef
    authUserStart(data: { login: string, pass: string }) {
        const body = data;
        return this.http.post(PATH + 'api-service/user-login', body);
            // .pipe(map((request) => {
            // const res: any = request;
            // if (request){
            //
            // }
            // return request;
        // }));
    }

    userStatus(): Observable<any>{
        return this.http.get(PATH + 'api-service/user-status').pipe(map(data => {
            return data;
        }));

    }
}
