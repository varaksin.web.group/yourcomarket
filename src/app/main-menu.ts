export const MAIN_MENU_ITEMS = [
    {
        id: '1',
        text: 'Генерувати документ',
        href: '/'
    },
    {
        id: '2',
        text: 'Як працюе сервіс',
        href: '/about-service'
    },
    {
        id: '3',
        text: 'Часті запитання',
        href: '/faq'
    },
];