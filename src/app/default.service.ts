import {Injectable} from '@angular/core';
import {Category} from './classes/category';
import {Document} from './classes/document';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../environments/environment';
import {Question, Questions} from './classes/questions';
import {response} from 'express';

const PATH = environment.restApiUrl;
const PORT = '';

// const CATEGORY_ARR: Category[];

@Injectable({
    providedIn: 'root'
})
export class DefaultService {


    constructor(private http: HttpClient) {
    }

    getVariantsForSearch(): Observable<string[]>{
        // tslint:disable-next-line:no-shadowed-variable
        return this.http.get(PATH + 'api-service/get-items-for-search-autoload').pipe(map((response) => {
            const result: string[] = [];

            if (response){
                const res: string[] = Object.values(response);
                res.forEach((value) => {
                    result.push(value);
                });
            }
            return result;
        }));
    }

    getDocumentsListBySearch(target: string): Observable<Document[]>{
       return  this.http.get(PATH + 'api-service/get-search/' + target).pipe(map((response) => {
           const docsList: Document[] = [];
           if (response){
                const res: any[] = Object.values(response);
               // tslint:disable-next-line:prefer-for-of
                for (let k = 0; k < res.length; k++) {
                    const doc: Document = new Document(res[k].id, res[k].cat, res[k].title);
                    doc.catTitle = res[k].cat_name;
                    doc.price = res[k].price | 0;


                    docsList.push(doc);
                }
                console.log('docsList', docsList);

            }
           return docsList;
        })
       );
    }

    getContentPageByNid(nid: string): Observable<Questions> {

        return this.http.get(PATH + 'api-service/get-content/' + nid).pipe(map((response: any) => {
            const result: Questions = new Questions(response.title, response.body);
            if (response.questions){
                    const questions: any[] = Object.values(response.questions);
                    const questArray: Question[] = [];
                    for (let i = 0; i < questions.length; i++){
                        const quest: Question = new Question(
                            questions[i].quest,
                            questions[i].answer,
                        );
                        questArray.push(quest);
                    }
                    result.questions = questArray;
                    console.log('content faq', result);
                }
            return result;
            })
        );
    }

    getCategory(): Observable<Category[]> {

        return this.http.get(PATH + 'api-service/get-allcategories' + PORT).pipe(map((responce) => {
            const result: Category[] = [];

            if (responce) {
                const cetArray = Object.values(responce);
                if (cetArray.length){
                    // tslint:disable-next-line:prefer-for-of
                    for (let i = 0; i < cetArray.length; i++) {
                        const tax: Category = new Category(+cetArray[i].tid, cetArray[i].name, cetArray[i].desc || '');

                        if (cetArray[i].children !== undefined) {
                            const children: Category[] = Object.values(cetArray[i].children);
                            console.log('children', children);

                            if (children != null && children.length > 0) {

                                children.forEach((item: Category) => {
                                    const subTax: Category = new Category(item.tid, item.name, item.desc || '');
                                    if (item.children) {
                                        const subTaxResult: Category[] = [];
                                        const subchildren: Category[] = Object.values(item.children);
                                        subchildren.forEach((subItem: Category) => {
                                            subTaxResult.push(new Category(subItem.tid, subItem.name, subItem.desc || ''));

                                        });

                                        item.children = subTaxResult;
                                    }

                                });

                                tax.children = children;

                            }

                        }

                        result.push(tax);
                    }
                }

            }
            console.log('result', result);

            return result;
        }));
    }

    getDocumentsInCategory(tid: number): Observable<Document[]> {

        return this.http.get(PATH + 'api-service/get-document-in-categories/' + tid + PORT).pipe(map((responce) => {
            const docsList: Document[] = [];
            const res: any[] = Object.values(responce);
            // console.log('res', res)
            for (let k = 0; k < res.length; k++) {
                const doc: Document = new Document(res[k].id, res[k].cat, res[k].title);
                doc.catTitle = res[k].cat_name;
                // if (tid == doc.catId) {
                docsList.push(doc);
                // }
            }
            return docsList;
        }));
    }

    getDocumentById(id: number): Observable<Document> {

        return this.http.get(PATH + 'api-service/get-document/' + id).pipe(map((responce: any) => {
            const responceArr = responce;
            // console.log('responce', responceArr.id);

            const doc: Document = new Document(+responceArr.id, +responceArr.cid, responceArr.title);
            doc.body = responceArr.body;
            doc.price = responceArr.price;

            return doc;
        }));
    }
}
