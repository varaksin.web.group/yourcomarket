import { Component, OnInit } from '@angular/core';
import {MAIN_MENU_ITEMS} from '../../main-menu';
import {MenuItem} from '../../classes/menu-item';

@Component({
  selector: 'app-menu-left',
  templateUrl: './menu-left.component.html',
  styleUrls: ['./menu-left.component.scss']
})
export class MenuLeftComponent implements OnInit {
    mainMenu!: MenuItem[];

  constructor() { }

  ngOnInit(): void {
    this.mainMenu = MAIN_MENU_ITEMS;
  }

}
