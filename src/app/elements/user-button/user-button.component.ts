import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {LoginFormComponent} from '../login-form/login-form.component';
import {AuthService} from '../../services/auth.service';
import {User} from "../../classes/user";
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-user-button',
  templateUrl: './user-button.component.html',
  styleUrls: ['./user-button.component.scss']
})
export class UserButtonComponent implements OnInit {
  typesOfShoes: string[] = ['user office', 'Clogs'];
  storageToken = false;
  userStatus: any = false;
  user!: User;


  constructor(
      public dialog: MatDialog,
      private authService: AuthService,
      private router: Router) { }
  ngOnInit(): void {
    if (localStorage.getItem('jwt-token')) {
      this.storageToken = true;
      let userStorage = localStorage.getItem('user-data');

      if (userStorage != null) {
        this.user = JSON.parse(userStorage);
      }
    } else {
      this.storageToken = false;
    }
    this.authService.userStatus().subscribe(data => {
      this.userStatus = data;
    });


  }

  openDialog(): void{
    const dialogRef = this.dialog.open(LoginFormComponent, {
      width: '400px',
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result){
        this.authService.authUserStart(result).subscribe(data => {
          const userData: any = data;
          if (userData.token){

            this.user = new User(userData.user.uid, userData.user.mail, userData.user.name);
            setTimeout(() => {
              localStorage.setItem('jwt-token', userData.token);
              localStorage.setItem('user-data', JSON.stringify(this.user));
              location.reload(true);
            }, 10);
          }

        });
      }
    });
  }



  onOpenLoginForm(){
    this.openDialog();
  }


  onLoginClose() {
    setTimeout(()=>{
      localStorage.removeItem('jwt-token');
      localStorage.removeItem('user-data');
      location.reload(true);
    }, 100);
  }

    onEditProfile() {
    this.router.navigate(['user']);

    }
}
