import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserButtonComponent} from './user-button/user-button.component';
import {MaterialModule} from '../material/material.module';
import {TermFormComponent} from './term-form/term-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {DocumentListComponent} from './document-list/document-list.component';
import {DocumentEditFormComponent} from './document-edit-form/document-edit-form.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {MatButtonModule} from '@angular/material/button';
import {ProfileCardComponent} from './profile-card/profile-card.component';
import {ProfileFormComponent} from './profile-form/profile-form.component';
import {FooterComponent} from './footer/footer.component';
import {LeftSidenowblockComponent} from './left-sidenowblock/left-sidenowblock.component';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {AppRoutingModule} from '../app-routing.module';
import {SearchBarComponent} from './search-bar/search-bar.component';
import {DocumentListTableComponent} from './document-list-table/document-list-table.component';
import { HeaderComponent } from './header/header.component';
import { MenuLeftComponent } from './menu-left/menu-left.component';


@NgModule({
    declarations: [
        UserButtonComponent,
        TermFormComponent,
        DocumentListComponent,
        DocumentEditFormComponent,
        LoginFormComponent,
        ProfileCardComponent,
        ProfileFormComponent,
        FooterComponent,
        LeftSidenowblockComponent,
        MainMenuComponent,
        SearchBarComponent,
        DocumentListTableComponent,
        HeaderComponent,
        MenuLeftComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        MatButtonModule,
        AppRoutingModule
    ],
    entryComponents: [
        LoginFormComponent,
    ],
    exports: [
        UserButtonComponent,
        TermFormComponent,
        DocumentListComponent,
        DocumentEditFormComponent,
        LoginFormComponent,
        ProfileCardComponent,
        ProfileFormComponent,
        FooterComponent,
        LeftSidenowblockComponent,
        MainMenuComponent,
        SearchBarComponent,
        DocumentListTableComponent,
        HeaderComponent,
        MenuLeftComponent
    ]
})
export class ElementsModule {
}
