import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DefaultService} from '../../default.service';
import {Document} from '../../classes/document';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, OnChanges {

  @Input()tid!: number;
  // documentList$!: Observable<Document[]>;
  documentList!: Document[];
  constructor(private service: DefaultService, private router: Router) { }

  ngOnInit(): void {

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.tid){
      // this.documentList$ = this.service.getDocumentsInCategory(this.tid);
      this.service.getDocumentsInCategory(this.tid).subscribe(data => {
        this.documentList = data;
      });
    }
  }

  selectDoc(id: number): void {
    this.router.navigate(['doc', id]);
  }
}
