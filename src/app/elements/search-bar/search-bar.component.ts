import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {searchSlide} from 'src/app/animations';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {DefaultService} from '../../default.service';

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss'],
    animations: [
        searchSlide
    ]
})
export class SearchBarComponent implements OnInit {
    searchForm!: FormGroup;
    showInput = false;
    searchValue!: string;
    myControl = new FormControl();
    options!: string[];
    filteredOptions!: Observable<string[]>;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private service: DefaultService) {
    }

    ngOnInit(): void {
        this.searchForm = this.fb.group({
            search: ['']
        });
        this.service.getVariantsForSearch().subscribe((items) => {
            if (items) { this.options = items; }
        });
        this.filteredOptions = this.myControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();

        return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }

    get searchInputValue(): { [p: string]: AbstractControl } {
        return this.searchForm.controls;
    }


    onClickSearchButton(): void {
        if (!this.showInput) {
            this.showInput = true;
        } else {
            console.log(this.searchInputValue);
            if (this.searchInputValue.search.value) {
                this.router.navigate(['search', this.searchInputValue.search.value]);
            } else {
                this.showInput = false;
            }
        }
    }

    onBlur(): void {
        this.searchValue = '';
        setTimeout(() => {
                this.showInput = false;
            },
            2000);
    }
}
