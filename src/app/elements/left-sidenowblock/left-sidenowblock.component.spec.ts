import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftSidenowblockComponent } from './left-sidenowblock.component';

describe('LeftSidenowblockComponent', () => {
  let component: LeftSidenowblockComponent;
  let fixture: ComponentFixture<LeftSidenowblockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeftSidenowblockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftSidenowblockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
