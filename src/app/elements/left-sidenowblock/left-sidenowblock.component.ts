import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import {Category} from '../../classes/category';
import {DefaultService} from '../../default.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-left-sidenowblock',
  templateUrl: './left-sidenowblock.component.html',
  styleUrls: ['./left-sidenowblock.component.scss']
})
export class LeftSidenowblockComponent implements OnInit {

  // categories: Category[];
  treeControl = new NestedTreeControl<Category>(node => node.children);
  dataSource = new MatTreeNestedDataSource<Category>();
  constructor(private service: DefaultService, public router: Router) { }
  ngOnInit(): void {
    this.service.getCategory().subscribe(data => {
      if (data) {
        this.dataSource.data = data;
      }
    });
  }
  hasChild = (_: number, node: Category) => !!node.children && node.children.length > 0;

  onGoToCategory(tid: number): void{
    this.router.navigate(['/category', tid]);
  }

  onToggleTree(node: Category): void {

  }

  onDownloadDocuments(node: Category): void {

  }
}
