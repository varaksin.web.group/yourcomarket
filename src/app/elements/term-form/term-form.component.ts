import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DefaultService} from '../../default.service';
import {MatSelectChange} from '@angular/material/select';
import {Observable} from 'rxjs';
import {Category} from '../../classes/category';

@Component({
  selector: 'app-term-form',
  templateUrl: './term-form.component.html',
  styleUrls: ['./term-form.component.scss']
})
export class TermFormComponent implements OnInit {
  @Output() categoryId: EventEmitter<number> = new EventEmitter();

  categoryForm: FormGroup = this.fb.group({
    parent: [],
    second: [],
  });

  descSelectCat = '';
  nameSelectCat = '';
  selectTerm = '';
  selectTermObj: any = Category;
  subSelectTermObj: any = Category;
  sendTid = 0;

  categoryList!: Observable<any[]> ;
  constructor(private fb: FormBuilder, private service: DefaultService) { }

  ngOnInit(): void {

    this.categoryList = this.service.getCategory();

  }


  SelectCategory(event: MatSelectChange): void{
    this.selectTermObj = event.value;
    if (this.selectTermObj.desc) {
      this.descSelectCat = this.selectTermObj.desc.charAt(0).toUpperCase() + this.selectTermObj.desc.slice(1);
    }
    this.nameSelectCat = this.selectTermObj.name.charAt(0).toUpperCase() + this.selectTermObj.name.slice(1);;

    this.selectTerm = event.value.name;
    this.categoryId.emit(event.value.tid);


  }

  SelectionCategory(event: MatSelectChange): void{
    this.subSelectTermObj = event.value;
    if (this.subSelectTermObj.desc) {
      this.descSelectCat = this.subSelectTermObj.desc;
    }
    this.nameSelectCat = this.subSelectTermObj.name.charAt(0).toUpperCase() + this.subSelectTermObj.name.slice(1);

    this.sendTid = event.value.tid;
    this.categoryId.emit(event.value.tid);
  }

  SelectionSubCategory(event: MatSelectChange) {
    if (event.value.desc) {
      this.descSelectCat = event.value.desc;
    }
    event.value.name.toLowerCase();
    this.nameSelectCat = event.value.name.charAt(0).toUpperCase() + event.value.name.slice(1);

    this.categoryId.emit(event.value.tid);

  }
}
