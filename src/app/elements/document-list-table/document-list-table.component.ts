import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Document} from '../../classes/document';

@Component({
  selector: 'app-document-list-table',
  templateUrl: './document-list-table.component.html',
  styleUrls: ['./document-list-table.component.scss']
})
export class DocumentListTableComponent implements OnInit, OnChanges {
    dataSource!: Document[];
    @Input() documents!: Document[];
  displayedColumns: string[] = ['name', 'category', 'actions'];

  constructor() { }

  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges): void {
      console.log('this.documents' , this.documents);
      this.dataSource = this.documents;
  }

}
