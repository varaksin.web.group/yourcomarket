import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {User} from '../../classes/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {
  @Input() user!: User;
  @Output() profileEditResult = new EventEmitter();
  profileForm!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.profileForm = this.fb.group({
      name: [this.user.name, Validators.required],
    });
  }

  get profileFormValues(): any{
    return this.profileForm.value;

  }

  onSaveProfile(): void{
    this.profileEditResult.emit(this.profileFormValues);

  }

  onStopEdit(): void{
    this.profileEditResult.emit(false);
  }
}
