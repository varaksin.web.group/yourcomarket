import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from '../../classes/user';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit, OnChanges {
  @Input() user!: User;
  editFormShow = false;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  onEditProfile(event: any): void {
    if (event) {
      setTimeout(() => {
        this.user.name = event.name;
      }, 10);
    }
    this.editFormShow = false;

  }

  onEditProfileStart(): void {
    this.editFormShow = true;
  }
}
