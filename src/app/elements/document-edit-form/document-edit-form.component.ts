import {Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DinamicFormFieldItem} from '../../classes/dinamic-form-field-item';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-document-edit-form',
  templateUrl: './document-edit-form.component.html',
  styleUrls: ['./document-edit-form.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'uk-UK'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class DocumentEditFormComponent implements OnInit, OnChanges {
  // formStatus = true;
  constructor(private fb: FormBuilder, private _adapter: DateAdapter<any>) { }

  get formValues(): any{
    return this.editForm.value;
  }
  @Input() fieldData!: DinamicFormFieldItem[];
  @Output() formFieldsValue = new EventEmitter();
  editForm!: FormGroup;

  startDate: any = new Date(Date.now());

  ukranian(){
    this._adapter.setLocale('uk');
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // this.startDate = new Date(1990, 0, 1);
    const controlConfig: any = {};
    if (this.fieldData){
      for (let k = 0; k < this.fieldData.length; k++){
        // console.log('this.fieldData' + k , this.fieldData[k]);
        controlConfig[this.fieldData[k].key] = [this.fieldData[k].value, Validators.required];
      }
    }
    // console.log('controlConfig', controlConfig);
    this.editForm = this.fb.group(controlConfig);
    // this.formStatus = !this.formStatus;

  }

  onSubmit(): void{
    this.formFieldsValue.emit(this.formValues);
    // this.formStatus = !this.formStatus;
  }
}
