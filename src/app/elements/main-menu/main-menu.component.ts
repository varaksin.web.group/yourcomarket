import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MenuItem} from '../../classes/menu-item';
import {MAIN_MENU_ITEMS} from '../../main-menu';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})

export class MainMenuComponent implements OnInit {
  public mainMenu!: MenuItem[];

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.mainMenu = MAIN_MENU_ITEMS;
  }

}
