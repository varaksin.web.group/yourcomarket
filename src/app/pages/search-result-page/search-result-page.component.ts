import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DefaultService} from '../../default.service';
import {Document} from '../../classes/document';
import {Observable} from 'rxjs';



@Component({
  selector: 'app-search-result-page',
  templateUrl: './search-result-page.component.html',
  styleUrls: ['./search-result-page.component.scss']
})
export class SearchResultPageComponent implements OnInit {
    searchTarget!: string;
  documents!: Document[];
  countDocuments = 0;

  constructor(private activateRoute: ActivatedRoute, private service: DefaultService) { }

  ngOnInit(): void {
    this.activateRoute.paramMap.subscribe((params) => {
      if (params){
        const paramsMap = Object.values(params);
        this.searchTarget = paramsMap[0].searchData;
        this.service.getDocumentsListBySearch(this.searchTarget).subscribe((items) => {
          this.documents = items;
          this.countDocuments = items.length ? items.length : 0;
        });
      }
    });
  }

}
