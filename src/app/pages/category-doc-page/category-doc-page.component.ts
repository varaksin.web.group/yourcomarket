import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DefaultService} from '../../default.service';
import {Document} from '../../classes/document';

@Component({
    selector: 'app-category-doc-page',
    templateUrl: './category-doc-page.component.html',
    styleUrls: ['./category-doc-page.component.scss']
})
export class CategoryDocPageComponent implements OnInit {
    public documentList!: Document[];
    public emptyMessage = 'Вибачте, аде документов не знайдено. Спробуйте ще.';
    public documentStatus = false;
    docCount = 0
    public categoryName!: string;

    constructor(private router: Router, private activatedRouter: ActivatedRoute, private service: DefaultService) {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    ngOnInit(): void {
        const tid = this.activatedRouter.snapshot.params.tid;
        this.service.getDocumentsInCategory(tid).subscribe((items) => {
            // items.forEach((item)=>{
            //   this.documentList.push(new Document(item.id,item.catId,item.name));
            // });
            this.documentList = [];
            if (items.length) {
                // this.categoryName = ;
                this.docCount = items.length;
                this.documentList = items;
                this.documentStatus = true;
            } else {
                this.documentStatus = false;
            }
        });
    }
}
