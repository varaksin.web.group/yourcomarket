import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryDocPageComponent } from './category-doc-page.component';

describe('CategoryDocPageComponent', () => {
  let component: CategoryDocPageComponent;
  let fixture: ComponentFixture<CategoryDocPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryDocPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryDocPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
