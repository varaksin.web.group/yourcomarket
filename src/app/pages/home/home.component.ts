import {Component, OnInit} from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    pageTitle: string = 'YourKoMarket';
    selectCategory: number = 0;

    constructor(private title: Title, private meta: Meta) {
    }

    ngOnInit(): void {
        this.title.setTitle(this.pageTitle);
        this.meta.addTags([
            {name: 'keywords', content: 'генерація юридичних документів'},
            {name: 'description', content: 'Сервіс генерації юридичних документів "ЮрКоМаркет"'},
            {name: 'robots', content: 'index, follow'}
        ]);
    }

    onAction() {

    }

    selectCat(event: any) {
        this.selectCategory = event;
    }
}
