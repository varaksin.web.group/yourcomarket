import { Component, OnInit } from '@angular/core';
import {DefaultService} from '../../default.service';
import {Question, Questions} from '../../classes/questions';

@Component({
  selector: 'app-faq-page',
  templateUrl: './faq-page.component.html',
  styleUrls: ['./faq-page.component.scss']
})
export class FaqPageComponent implements OnInit {
  public questions!: Questions;
  public title!: string;
  public body!: string;
  public questionArray!: Question[];
  public show = false;

  constructor(private service: DefaultService) { }

  ngOnInit(): void {
    this.service.getContentPageByNid('2').subscribe((items) => {
      if (items) {
        this.show = true;
        this.title = items.title;
        this.body = items.body;
        this.questionArray = items.questions;
      }

    });
  }

}
