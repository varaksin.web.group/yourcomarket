import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {ElementsModule} from '../elements/elements.module';
import { DocumentPageComponent } from './document-page/document-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {MaterialModule} from '../material/material.module';
import { UserPageComponent } from './user-page/user-page.component';
import { FaqPageComponent } from './faq-page/faq-page.component';
import { AboutServicePageComponent } from './about-service-page/about-service-page.component';
import { CategoryDocPageComponent } from './category-doc-page/category-doc-page.component';
import {AppRoutingModule} from '../app-routing.module';
import { SearchResultPageComponent } from './search-result-page/search-result-page.component';


@NgModule({
    // tslint:disable-next-line:max-line-length
    declarations: [
        HomeComponent,
        DocumentPageComponent,
        PageNotFoundComponent,
        UserPageComponent,
        FaqPageComponent,
        AboutServicePageComponent,
        CategoryDocPageComponent,
        SearchResultPageComponent
    ],
    exports: [
        HomeComponent,
        DocumentPageComponent,
        PageNotFoundComponent,
        UserPageComponent,
        FaqPageComponent,
        AboutServicePageComponent,
        CategoryDocPageComponent,
        SearchResultPageComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        ElementsModule,
        AppRoutingModule
    ]
})
export class PagesModule { }
