import { Component, OnInit } from '@angular/core';
import {DefaultService} from '../../default.service';

@Component({
  selector: 'app-about-service-page',
  templateUrl: './about-service-page.component.html',
  styleUrls: ['./about-service-page.component.scss']
})
export class AboutServicePageComponent implements OnInit {
  body!: string;
  title!: string;

  constructor(private service: DefaultService) { }

  ngOnInit(): void {
    this.service.getContentPageByNid('3').subscribe((items) => {
      if (items){
        this.title = items.title;
        this.body = items.body;
      }
    });
  }

}
