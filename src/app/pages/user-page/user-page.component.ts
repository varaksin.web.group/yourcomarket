import { Component, OnInit } from '@angular/core';
import {User} from '../../classes/user';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  user!: User;
  constructor() { }

  ngOnInit(): void {
    if (localStorage.getItem('user-data')) {

      const userData = localStorage.getItem('user-data');
      if (typeof userData === 'string') {
        this.user = JSON.parse(userData);
      }

    }
  }

}
