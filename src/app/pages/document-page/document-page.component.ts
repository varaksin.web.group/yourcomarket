import { Component, OnInit } from '@angular/core';
import {DefaultService} from '../../default.service';
import {Document} from '../../classes/document';
import {ActivatedRoute} from '@angular/router';
import {ExtractDocumentService} from '../../extract-document.service';
import {DinamicFormFieldItem} from '../../classes/dinamic-form-field-item';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-document-page',
  templateUrl: './document-page.component.html',
  styleUrls: ['./document-page.component.scss']
})
export class DocumentPageComponent implements OnInit {
  document!: Document;
  showDoc = false;
  formString!: DinamicFormFieldItem[];
  docText!: string;
  documentFormValues!: any[];
  pageViewStatus = false;

  constructor(
      private service: DefaultService,
      private extractDoc: ExtractDocumentService,
      private activatedRoute: ActivatedRoute,
      private titleService: Title,
      private metaService: Meta) { }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe((params) => {
      const paramsMap = Object.values(params);

      this.service.getDocumentById(paramsMap[0].id).subscribe((item) => {
        this.document = item;
        if(item) {
          this.pageViewStatus = true;
        }
        // console.log('document', item);
        this.titleService.setTitle(item.title);
        this.metaService.addTags([
          {name: 'keywords', content: 'генерація ' + item.title},
          {name: 'description', content: 'Сервіс генерації ' + item.title},
          {name: 'robots', content: 'index, follow'}
        ]);
        this.formString = this.extractDoc.onExtractDocumentBody(item.body);
      });

    });

  }

  onGetFormData(event: any): void{
    this.documentFormValues = event;
    this.docText = this.extractDoc.onExtractDocReplaceString(this.document.body, event);
    this.showDoc = !this.showDoc;
  }

  onEditDoc(): void {
    this.formString = this.extractDoc.onExtractDocumentBody(this.document.body, this.documentFormValues);

    this.showDoc = !this.showDoc;

  }
}
