import {Injectable} from '@angular/core';
import * as _moment from 'moment';
import {DinamicFormFieldItem} from './classes/dinamic-form-field-item';

@Injectable({
    providedIn: 'root'
})
export class ExtractDocumentService {

    constructor() {
    }

    onExtractDocumentBody(html: string, values?: any) {
        const regexp = /\*(.*?)\*/g;

        const matchAll: any = html.match(regexp);
        let valuesArr = values ? Object.values(values) : [];

        const formFieldArr: DinamicFormFieldItem[] = [];
        if (matchAll) {
            for (let i = 0; i < matchAll.length; i++) {

                let value: any = valuesArr ? valuesArr[i] : '';
                matchAll[i] = matchAll[i].substring(0, matchAll[i].length - 2);
                matchAll[i] = matchAll[i].substring(2);
                const matchObj = matchAll[i];
                const indexNum = matchObj.indexOf(':');
                const key = matchObj.substring(0, indexNum);
                let type =  'text';
                let label = matchObj.substring(indexNum + 1);
                if (label.indexOf(':') != -1) {
                    type =  label.substring(label.indexOf(':') + 1);
                    label = label.substring(0 ,label.length - type.length);
                }
                formFieldArr.push(new DinamicFormFieldItem(value, key, label, type));
            }
        }
        return formFieldArr;
    }

    onExtractDocReplaceString(html: string, event: {}): string{
        const regexp = /\*(.*?)\*/g;

        const matchAll: any = html.match(regexp);
        let eventArr: string[] = Object.values(event);
        let replaceArr: any[] = [];
        let extractBody = html;
        if (matchAll) {
            for (let i = 0; i < matchAll.length; ++i) {
                let substring = matchAll[i];
                let replaceVal = eventArr[i];
                if (substring.indexOf('calendar') != -1){
                    replaceVal = _moment(replaceVal).locale('uk').format('LL');
                }
                extractBody =  extractBody.replace(substring, replaceVal)
            }
        }
        return extractBody;
    }
}
