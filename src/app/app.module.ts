import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {PagesModule} from './pages/pages.module';
import {ElementsModule} from './elements/elements.module';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AuthGuard} from './guards/auth.guard';
import {MAT_DATE_LOCALE} from '@angular/material/core';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'serverApp'}),
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        PagesModule,
        ElementsModule,
        AppRoutingModule,
    ],
    providers: [AuthGuard,
        {provide: MAT_DATE_LOCALE, useValue: 'ua-UA'}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
