
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDividerModule} from '@angular/material/divider';
import {MatNativeDateModule} from '@angular/material/core';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTreeModule} from '@angular/material/tree';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTableModule} from '@angular/material/table';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

// import {MatButtonModule} from '@angular/material/button';


// noinspection AngularInvalidImportedOrDeclaredSymbol
@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MatCardModule,
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
        MatListModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatDialogModule,
        MatDatepickerModule,
        MatDividerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatMomentDateModule,
        MatSidenavModule,
        MatTreeModule,
        MatExpansionModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatAutocompleteModule,
        // MatButtonModule,
    ], exports: [
        MatCardModule,
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
        MatListModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatDialogModule,
        MatDatepickerModule,
        MatDividerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatMomentDateModule,
        MatSidenavModule,
        MatTreeModule,
        MatExpansionModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatAutocompleteModule,
        // MatButtonModule
    ]
})
export class MaterialModule {
}
