import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {DocumentPageComponent} from './pages/document-page/document-page.component';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {UserPageComponent} from './pages/user-page/user-page.component';
import {AuthGuard} from './guards/auth.guard';
import {AboutServicePageComponent} from './pages/about-service-page/about-service-page.component';
import {FaqPageComponent} from './pages/faq-page/faq-page.component';
import {CategoryDocPageComponent} from './pages/category-doc-page/category-doc-page.component';
import {SearchResultPageComponent} from './pages/search-result-page/search-result-page.component';

const routes: Routes = [
    {path: '', component: HomeComponent, data: {animation: 'homePage'}},
    {path: 'doc/:id', component: DocumentPageComponent},
    {path: 'about-service', component: AboutServicePageComponent, data: {animation: 'aboutPage'}},
    {path: 'category/:tid', component: CategoryDocPageComponent},
    {path: 'search/:searchData', component: SearchResultPageComponent},
    {path: 'faq', component: FaqPageComponent, data: {animation: 'faqPage'}},
    {path: 'user', component: UserPageComponent, canActivate: [AuthGuard]},
    {path: '', redirectTo: '/', pathMatch: 'full'}, // redirect to `first-component`
    {path: '**', component: PageNotFoundComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        initialNavigation: 'enabled'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
